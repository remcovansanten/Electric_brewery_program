//---------------------------------------------------------------------------------------------------------------------
// Function: Pump system
//---------------------------------------------------------------------------------------------------------------------

int PumpSystem(double temperature) {
  // Function to activate the pump to mix the wort in the system
  // As long as the temperature is lower than 100 degrees, the pummp should be turned on
  if (temperature < 95) {
    return 1;
  }
  else {
    return 0;
  }
}

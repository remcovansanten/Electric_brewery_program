//---------------------------------------------------------------------------------------------------------------------
// Function: Relay control
//---------------------------------------------------------------------------------------------------------------------

void RelayControl(int relay1_int, int relay2_int) {
  if (relay1_int == 1) {
    digitalWrite(relay1, LOW);
  } else {
    digitalWrite(relay1, HIGH);
  }

  if (relay2_int == 1) {
    digitalWrite(relay2, LOW);
  } else {
    digitalWrite(relay2, HIGH);
  }
}

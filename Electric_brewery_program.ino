/*
   Student: Aaron van Santen - S2282887
   This project is created for an assignment of the University of Twente
   The project aims to create an interface for an automatic brewery. The user can control up to 9 different brew stages witht their own temperature and time
*/

/*-------------------------
  LIBRARIES USED IN THE PROGRAM
  -------------------------*/
#include <SD.h>
#include <DallasTemperature.h>
#include <PID_v1.h>
#include <LiquidCrystal_I2C.h>
// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include "RTClib.h"
RTC_DS1307 rtc;

/*----------------------------
DEBUG deinition
-----------------------------*/
#define DEBUG 1

/*-------------------------
  INITIATE OBJECTS
  -------------------------*/
// Data wire is pin 2
#define ONE_WIRE_BUS 2

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

//// Create the file
//File myFile = SD.open("DataLog.txt", FILE_WRITE);

// Initlialize the display
LiquidCrystal_I2C lcd(0x27, 16, 2);

/*-------------------------
  DEFINING REQUIRED ITEMS
  -------------------------*/
//Way to hardcode the variables
double Input, Output, Setpoint;
// Aggressive parameters to increase temperature quickly.   NEED TO BE CHANGED
double AggKp = 80, AggKi = 20, AggKd = 30;
// Conservative parameters to increase temperature slower.  NEED TO BE CHANGED
double ConKp = 2, ConKi = 1, ConKd = 0;

// Create the PID loop based on the aggresive paramaters.
PID controlPID(&Input, &Output, &Setpoint, AggKp, AggKi, AggKd, DIRECT);

// Define time interval
double intervaltime = 10000;

// Get current time
unsigned long timestart = millis();

// Create arrays for the different temperatures and times for the stages
double Temperature[] = {0};
double KeepWarmTime[] = {0};
double amount_of_stages;

// Define variables used to claculate the temperature of the thermisistor
int Vo;
const float R1 = 10000;
float logR2, R2, T;
const float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

// Define finish time
unsigned long finish_time = 0;

/*-------------------------
  Fucntion prototypes
  -------------------------*/
void StageCreation();
bool processWarm(int stage);
bool processKeepWarm(int stage);
void RelayControl(int relay1, int relay2);
int PumpSystem(double temperature);

/*-------------------------
  ARDUINO PINS
  -------------------------*/
// Output pins
const int relay1 = 3;
const int relay2 = 4;

const int relay_pump; // DEFINE THIS PIN LATER ON

const int red_pin = 6;
const int green_pin = 7;
const int blue_pin = 8;

const int SD_card = 53;

// Input pins
const int ThermistorPin = A0;
const int encoderpin = A1;
const int buttonpin = A2;

/*-------------------------
  DEFINE FUNCTIONS USED IN THE PROGRAM
  ---------------------------*/

//---------------------------------------------------------------------------------------------------------------------
// Function: Thermistor temperature
//---------------------------------------------------------------------------------------------------------------------

float thermistor_temperature()
{
  Vo = analogRead(ThermistorPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));
  T = T - 273.15;
  return T;
}

//---------------------------------------------------------------------------------------------------------------------
// Function: LED Management
//---------------------------------------------------------------------------------------------------------------------

void LEDManagement(int red_light, int blue_light, int green_light)
{
  // Funtion to manage the colour of the LED. This will be used to mark the different processes
  analogWrite(blue_pin, blue_light);
  analogWrite(green_pin, green_light);
  analogWrite(red_pin, red_light);
}



/*-------------------------
  LOOP SECTION OF THE PROGRAM
  ---------------------------*/
void loop()
{
  
  bool stage_completion = false;
  bool time_completion = false;
  for (int stage = 0; stage <= 1; stage++)
  { finish_time = 0;
    if (stage == 2)
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("brewing");
      lcd.setCursor(0, 1);
      lcd.print("completed");
      Serial.print("Brewing completed");
      delay(2500);
      lcd.clear();
      exit(0);
    }
    Serial.print("Starting Stage: "); Serial.println(stage);
    while (stage_completion != true)
    {
      stage_completion = processWarm(stage);
    }
    while (time_completion != true)
    {
      time_completion = processKeepWarm(stage);
    }
  }
  stage_completion = false;
  time_completion = false;
}
/*-------------------------
  END OF PROGRAM
  -------------------------*/

//---------------------------------------------------------------------------------------------------------------------
// Function: Keep warm
//---------------------------------------------------------------------------------------------------------------------

bool processKeepWarm(int stageWarm)
{
#ifdef DEBUG
  Serial.print("entered process processKeepwarm for stage: ");
  Serial.println(stageWarm);
#endif

  // Get the current time of the stage
  DateTime now = rtc.now();
  unsigned long current_time = now.unixtime();

  // If not defined, define a finish time.
  if (finish_time == 0)
  {
    finish_time = (current_time + (int(KeepWarmTime[stageWarm] * 60)));
#ifdef DEBUG
    Serial.println("Define Finish time");
    Serial.print("current time: ");
    Serial.println(long(current_time));
    Serial.print("KeepWarmTime: ");
    Serial.println(long(KeepWarmTime[stageWarm]));
    Serial.print("Finish time : ");
    Serial.println(long(finish_time));
#endif
  }

  while (current_time < finish_time)
  {
#ifdef DEBUG
    Serial.println("check Finish time");
    Serial.print("current time: ");
    Serial.println(long(current_time));
    Serial.print("KeepWarmTime: ");
    Serial.println(long(KeepWarmTime[stageWarm]));
    Serial.print("Finish time : ");
    Serial.println(long(finish_time));
#endif
    sensors.requestTemperatures();
    Input = sensors.getTempCByIndex(0);
    double gap = Temperature[stageWarm] - Input;

    lcd.clear();

    // Check if the window needs to be shifted
    if (millis() - timestart > intervaltime)
    {
#ifdef DEBUG
      Serial.println("Windows shifted");
      Serial.print("millis    : ");
      Serial.println(long(millis));
      Serial.print("timestart : ");
      Serial.println(long(timestart));
      Serial.print("millis- ts: ");
      Serial.println(long(millis - timestart));
      Serial.print("int time  : ");
      Serial.println(long(intervaltime));
#endif
      timestart += intervaltime;
      // Compute the PID Time
      controlPID.Compute();
    }

    // Check if the pump need to be active
    switch (PumpSystem(Input))
    {
    case 0:
      digitalWrite(relay_pump, LOW);
    case 1:
      digitalWrite(relay_pump, HIGH);
    }

    // Check if time has passed
    DateTime now = rtc.now();
    unsigned long time_left = (finish_time - now.unixtime());

    // Print the temperature to the serial monitor
    Serial.print("Temperature /Input: ");
    Serial.print(Input);
    Serial.print("   ");
    Serial.print("Output: ");
    Serial.print(Output);
    Serial.print("   ");
    Serial.print("Setpoint: ");
    Serial.print(Setpoint);
    Serial.print("   ");
    Serial.print("Gap: ");
    Serial.print(gap);
    Serial.print("   ");
    Serial.print("time left: ");
    Serial.println(time_left);

    //RvS Begin
    File myFile = SD.open("DataLog.txt", FILE_WRITE);
    if (myFile)
    {
      myFile.print(Input);
      myFile.println(";");
      myFile.close();
#ifdef DEBUG1
      Serial.print("Temperature written to SD Card ");
#endif
    }
    else
    {
      Serial.println("Error opening file");
    }
    // Print the required information on the display
    lcd.setCursor(0, 0);
    lcd.print("temp:");
    lcd.setCursor(10, 0);
    lcd.print(Input);

    lcd.setCursor(0, 1);
    lcd.print("time left:");
    lcd.setCursor(10, 1);
    lcd.print(time_left);

    // We only use 1 relay here to keep the tmperature more constant
    if (Output < millis() - timestart && gap > 0)
    {
      controlPID.SetTunings(ConKp, ConKi, ConKd);
      RelayControl(1, 0);
      Serial.println(" Relay 1 is on  / Relay 2 is off");
    }
    else
    {
      RelayControl(0, 0);
      Serial.println(" Relay 1 is off / Relay 2 is off");
      {
        return false;
      }
    }
  }
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Stage completed");
  Serial.println("-------------------------------------------------------------------");
  Serial.println("Stage finished");

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Stage done");
  finish_time = 0;
  return true;
}

/*-------------------------
  SETUP OF THE PROGRAM
  ---------------------------*/
void setup()
{
    // Setup of the program
    Serial.begin(9600);

#ifndef ESP8266
    while (!Serial)
        ; // wait for serial port to connect. Needed for native USB
#endif

    if (!rtc.begin())
    {
        Serial.println("Couldn't find RTC");
        Serial.flush();
        abort();
    }

    if (!rtc.isrunning())
    {
        Serial.println("RTC is NOT running, let's set the time!");
        // When time needs to be set on a new device, or after a power loss, the
        // following line sets the RTC to the date & time this sketch was compiled
        rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
        // This line sets the RTC with an explicit date & time, for example to set
        // January 21, 2014 at 3am you would call:
        // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
    }

    // Set the Pinmodes
    pinMode(blue_pin, OUTPUT);
    pinMode(green_pin, OUTPUT);
    pinMode(red_pin, OUTPUT);
    pinMode(encoderpin, INPUT);
    pinMode(buttonpin, INPUT);
    pinMode(relay1, OUTPUT);
    pinMode(relay2, OUTPUT);
    pinMode(relay_pump, OUTPUT);

    digitalWrite(relay1, HIGH);
    digitalWrite(relay2, HIGH);

    // Start the sensors
    sensors.begin();

    // PID setup
    controlPID.SetMode(AUTOMATIC);
    controlPID.SetOutputLimits(0, intervaltime);
    controlPID.SetSampleTime(intervaltime);

    // Start the LCD
    lcd.init();
    lcd.backlight();
    lcd.clear();

    // see if the card is present and can be initialized:
    if (!SD.begin(SD_card))
    {
        Serial.println("Card failed, or not present");
        // don't do anything more:
        lcd.setCursor(0, 0);
        lcd.print("Card not found");
        lcd.setCursor(0, 1);
        lcd.print("--------------");
        while (1)
            ;
    }
    Serial.println("card initialized.");

    // Create the stages for the brew process
    StageCreation();

    Serial.println("--------------------------------------------------------------");
    Serial.println("Setup finished");
    lcd.setCursor(0, 0);
    lcd.print("Setup finished");
    lcd.setCursor(0, 1);
    lcd.print("Starting now");
    lcd.clear();
}

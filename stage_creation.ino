//---------------------------------------------------------------------------------------------------------------------
// Function: STAGE CREATION
//---------------------------------------------------------------------------------------------------------------------

void StageCreation() {
  #ifdef DEBUG1
    Serial.println("entered stagecreation");
  #endif

  bool number_set = false;
  double value_encoder, value_button;
  int threshold = 850;

  // Function to create all the different stages of the brewing process
  while (number_set != true) {
    value_encoder = analogRead(encoderpin);
    value_button = analogRead(buttonpin);

    double percentage = value_encoder / 1023;
    long stages = 1 + 8 * percentage;
    lcd.setCursor(0, 0);  lcd.print("Number of stages");
    lcd.setCursor(0, 1);  lcd.print(stages);

    if (value_button > threshold) {
      amount_of_stages = stages;
      Serial.print("amount_of_stages is set to: "); Serial.println(amount_of_stages);
      lcd.setCursor(4, 1);  lcd.print("Set");
      number_set = true;
      delay(1000);
    }
  }

  // Clear the LCD
  lcd.clear();

  for (int j = 0; j < amount_of_stages; j++) {
    bool time_set = false;
    bool temp_set = false;

    lcd.clear();

    while (temp_set != true) {
      // Caclulate the temperature based on the input of the potentiometer
      value_encoder = analogRead(encoderpin);
      value_button = analogRead(buttonpin);
      int temp_calc = 30 + 70 * (value_encoder / 1023);

      // Print info on the lcd
      lcd.setCursor(0, 0);  lcd.print("Temp stage "); lcd.print(j); lcd.print(":");
      lcd.setCursor(0, 1);  lcd.print(temp_calc);

      if (value_button > threshold) {
        Temperature[j] = temp_calc;
        Serial.print("Stage "); Serial.print(j); Serial.print(" Temperature is set to: "); Serial.print(Temperature[j]); Serial.println(" degrees celcius");
        lcd.setCursor(6, 1);  lcd.print("Set");
        temp_set = true;
        delay(1000);
      }
    }

    lcd.clear();

    while (time_set != true) {
      value_encoder = analogRead(encoderpin);
      value_button = analogRead(buttonpin);
      int time_calc = 60 * (value_encoder / 1023);

      lcd.setCursor(0, 0);  lcd.print("Time stage "); lcd.print(j); lcd.print(":");
      lcd.setCursor(0, 1);  lcd.print(time_calc);

      if (value_button > threshold) {
        KeepWarmTime[j] = time_calc;
        Serial.print("Stage "); Serial.print(j); Serial.print(" KeepWarmTime is set to: "); Serial.print(KeepWarmTime[j]); Serial.println(" minutes");
        time_set = true;
        lcd.setCursor(6, 1);  lcd.print("Set");
        delay(1000);
      }
    }
  }

  lcd.clear();
  lcd.setCursor(1, 0);  lcd.print("Stages set");

  Serial.println("----------------------------------------------------------------");

  Serial.println("Overview of the stages");
  for (int i = 0; i < amount_of_stages; i++) {
    Serial.print("Stage "); Serial.print(i + 1);
    Serial.print(" ->      Temperature: ");   Serial.print(Temperature[i]);  Serial.print(" degrees Ceclius");
    Serial.print(" ->      Time       : ");   Serial.print(KeepWarmTime[i]); Serial.println(" minutes");
  }
}

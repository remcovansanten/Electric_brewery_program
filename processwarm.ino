//---------------------------------------------------------------------------------------------------------------------
// Function: Process warm
//---------------------------------------------------------------------------------------------------------------------
bool processWarm(int stage) {
  #ifdef DEBUG
    Serial.print("entered process processwarm for stage: "); Serial.println(stage);
  #endif

  Setpoint = Temperature[stage];
  sensors.requestTemperatures();
  Input = sensors.getTempCByIndex(0);

  // Function to control get the temperature during the warming process
  // Read the temperature
  double gap = Setpoint - Input;

  // Create a non blocking delay
  // --------------------------------------------------------------------------------
  // Check if the window needs to be shifted
  if (millis() - timestart > intervaltime) {
#ifdef DEBUG
      Serial.println("Windows shifted");
      Serial.print("millis    : ");
      Serial.println(long(millis));
      Serial.print("timestart : ");
      Serial.println(long(timestart));
      Serial.print("millis- ts: ");
      Serial.println(long(millis - timestart));
      Serial.print("int time  : ");
      Serial.println(long(intervaltime));
#endif
    timestart = timestart + intervaltime;
    // Compute the PID Time
    controlPID.Compute();
    // Serial.println("-------------------- INTERVAL PASSED --------------------");
  }

  // Check if the pump need to be active
    switch (PumpSystem(Input)) {
    case 0: digitalWrite(relay_pump, LOW);
    case 1: digitalWrite(relay_pump, HIGH);
    }

  // Print the temperature to the serial monitor
  Serial.print("Temperature /Input: "); Serial.print(Input); Serial.print("   ");
  Serial.print("Output: "); Serial.print(Output); Serial.print("   ");
  Serial.print("Setpoint: "); Serial.print(Setpoint); Serial.print("   ");
  Serial.print("Gap: "); Serial.print(gap); Serial.print("   ");
  Serial.print("Time interval: "); Serial.print(timestart); Serial.print("   ");
  Serial.print("Current time: "); Serial.print(millis() - timestart);
    
  // Check to activate the relays
  // A big gap means both relays are on, a smaller gap means only 1 will turn on
  if (Output > millis() - timestart && gap > 3) {
    controlPID.SetTunings(AggKp, AggKi, AggKd);
    RelayControl(1, 1);
    Serial.print("   "); Serial.println("Relay 1 is on  / Relay 2 is on");
  } else if (Output > millis() - timestart && gap < 3 && gap > 0) {
    //    controlPID.SetTunings(ConKp, ConKi, ConKd);
    RelayControl(1, 0);
    Serial.print("   "); Serial.println("Relay 1 is on  / Relay 2 is off");
  } else {
    RelayControl(0, 0);
    Serial.print("   "); Serial.println("Relay 1 is off / Relay 2 is off");
  }
  
  //RvS Begin
  File myFile = SD.open("DataLog.txt", FILE_WRITE);
  if (myFile) {
    myFile.print(Input); myFile.print(";"); myFile.print(Setpoint) ;
    myFile.println(";");
    myFile.close();
  }
  else {
    Serial.println("Error opening file");
  }
  delay(1000);
  //RvS End

  // Print the required information on the display
  lcd.setCursor(0, 0);  lcd.print("live temp:");
  lcd.setCursor(10, 0); lcd.print(Input);

  lcd.setCursor(0, 1);  lcd.print("target:");
  lcd.setCursor(10, 1); lcd.print(Temperature[stage]);


  if (Input >= Temperature[stage]) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Temp reached");
    return true;
  } else {
    return false;
  }
}


